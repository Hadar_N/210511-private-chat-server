import express from 'express';
import http from 'http';
import { Server } from "socket.io";

const app = express();
const server = http.createServer(app);

const io = new Server(server);

app.get('/', (req, res) => {
  //res.sendFile(process.cwd() + '/index.html');
  res.status(200).send("server started");
});

const roomName=["room1","room2"];

io.on('connection', (socket) => {
    const myRoom= roomName[Math.floor(Math.random() * roomName.length)];
    socket.join(myRoom);
    console.log('a user connected to room:', myRoom);

    socket.on('disconnect', () => {
        socket.leave(myRoom);
        console.log('user disconnected from room:', myRoom);
      });

      socket.on('chat message', (msg) => {
        io.to(myRoom).emit('chat message', msg);
      });
  });


server.listen(3030, () => {
  console.log('listening on localhost:3030');
});